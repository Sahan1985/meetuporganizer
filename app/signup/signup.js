'use strict';
var formScope={};
angular.module('meetuporganizer.signup', [])

.config(function($stateProvider) {
    $stateProvider
        .state('signup', {
            url: "/signup",
            templateUrl: "signup/signup.html",
            controller: 'SignUpCtrl'
        })
})
.directive('passwordStrengthBar', function () {
    return {
        replace: true,
        restrict: 'E',
        template: '<div id="strength">' +
            '<small>Password strength:</small>' +
            '<ul id="strengthBar">' +
            '<li class="point"></li><li class="point"></li><li class="point"></li><li class="point"></li><li class="point"></li>' +
            '</ul>' +
            '</div>',
        link: function (scope, iElement, attr) {
            var strength = {
                colors: ['#F00', '#F90', '#FF0', '#9F0', '#0F0'],
                mesureStrength: function (p) {

                    var _force = 0;
                    var _regex = /[$-/:-?{-~!"^_`\[\]]/g; // "

                    var _lowerLetters = /[a-z]+/.test(p);
                    var _upperLetters = /[A-Z]+/.test(p);
                    var _numbers = /[0-9]+/.test(p);
                    var _symbols = _regex.test(p);

                    var _flags = [_lowerLetters, _upperLetters, _numbers, _symbols];
                    var _passedMatches = $.grep(_flags, function (el) {
                        return el === true;
                    }).length;

                    _force += 2 * p.length + ((p.length >= 10) ? 1 : 0);
                    _force += _passedMatches * 10;

                    // penality (short password)
                    _force = (p.length <= 6) ? Math.min(_force, 10) : _force;

                    // penality (poor variety of characters)
                    _force = (_passedMatches === 1) ? Math.min(_force, 10) : _force;
                    _force = (_passedMatches === 2) ? Math.min(_force, 20) : _force;
                    _force = (_passedMatches === 3) ? Math.min(_force, 40) : _force;

                    return _force;

                },
                getColor: function (s) {

                    var idx = 0;
                    if (s <= 10) {
                        idx = 0;
                    }
                    else if (s <= 20) {
                        idx = 1;
                    }
                    else if (s <= 30) {
                        idx = 2;
                    }
                    else if (s <= 40) {
                        idx = 3;
                    }
                    else {
                        idx = 4;
                    }

                    return { idx: idx + 1, col: this.colors[idx] };
                }
            };

            scope.$watch(attr.passwordToCheck, function (password) {
                if (password) {
                    var c = strength.getColor(strength.mesureStrength(password));
                    iElement.removeClass('ng-hide');
                    iElement.find('ul').children('li')
                        .css({ 'background': '#DDD' })
                        .slice(0, c.idx)
                        .css({ 'background': c.col });
                }
            });
        }
    };
})
.directive('match', function() {
    return {
        require: 'ngModel',
        restrict: 'A',
        scope: {
            match: '='
        },
        link: function(scope, elem, attrs, ctrl) {
            scope.$watch(function() {
                var modelValue = ctrl.$modelValue || ctrl.$$invalidModelValue;
                return (ctrl.$pristine && angular.isUndefined(modelValue)) || scope.match === modelValue;
            }, function(currentValue) {
                ctrl.$setValidity('match', currentValue);
            });
        }
    };
})
.controller('SignUpCtrl', function ($rootScope,$scope,$state) {
        var panel = this;

        var multiplyByItself = function() {
            var birth,myAge;
            birth = $('#dob').val();
            var today = new Date();
            var curr_date = today.getDate();
            var curr_month = today.getMonth() + 1;
            var curr_year = today.getFullYear();

            var pieces = birth.split('/');
            var birth_date = pieces[0];
            var birth_month = pieces[1];
            var birth_year = pieces[2];

            if (curr_month == birth_month && curr_date >= birth_date){
                myAge = parseInt(curr_year-birth_year) ;
            }
            else if (curr_month == birth_month && curr_date < birth_date){
                myAge = parseInt((curr_year-birth_year)-1);
            }
            else if (curr_month >= birth_month){
                myAge = parseInt(curr_year-birth_year);
            }
            else if (curr_month < birth_month) {
                myAge = parseInt((curr_year-birth_year)-1);
            }
            if(myAge < 18){
                alert('Minimum age should be 18.You have no access to sign up');
                $scope.selectedDate="";
            }

        };
        $scope.$watch('selectedDate', multiplyByItself);

        $rootScope.currentUser = Parse.User.current();

        //clear this after testing, all should be blank
        $scope.user = {
            email: 'admin@localhost.com',
            first_name: 'Admin',
            last_name: 'User',
            password: '',
            confirmPassword: ''
        };
        $scope.selectedCountry = "LK";
        $scope.selectedDate = "";

        $scope.signup = function () {
            var user = new Parse.User();

            user.set("email", $scope.user.email);
            user.set("username", $scope.user.email);
            user.set("first_name", $scope.user.first_name);
            user.set("last_name", $scope.user.last_name);
            user.set("password", $scope.user.password);
            user.set("dob", $scope.selectedDate);
            user.set("country", $scope.selectedCountry);
            $rootScope.dataLoaded = false;
            user.signUp(null, {
                success: function(user) {
                    $rootScope.currentUser = user;
                    $rootScope.$apply();
                    $rootScope.isLoggedIn = true;
                    $rootScope.dataLoaded = true;
                    $state.go('home');
                },
                error: function(user, error) {
                    $rootScope.isLoggedIn = false;
                    $rootScope.dataLoaded = true;
                    $state.go('signup');
                    alert("Unable to sign up:  " + error.code + " " + error.message);
                }
            });
        }
});
