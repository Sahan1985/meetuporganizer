'use strict';

angular.module('meetuporganizer.logout', [])

.config(function($stateProvider) {
    $stateProvider
        .state('logout', {
            url: "/logout",
            templateUrl: "logout/logout.html",
            controller: 'LogoutCtrl'
        })
})

.controller('LogoutCtrl', function ($rootScope,$scope,$state) {
    /*Parse LogOut Method*/
    Parse.User.logOut();

    $rootScope.currentUser = null;
    $rootScope.isLoggedIn = false;
    $state.go('login');
});