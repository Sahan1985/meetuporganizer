'use strict';

angular.module('meetuporganizer.version', [
  'meetuporganizer.version.interpolate-filter',
  'meetuporganizer.version.version-directive'
])

.value('version', '0.1');
