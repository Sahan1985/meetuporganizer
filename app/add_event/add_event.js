'use strict';

angular.module('meetuporganizer.add_event', ['ngRoute'])

    .config(function ($stateProvider) {
        $stateProvider
            .state('add_event', {
                url: "/add_event",
                templateUrl: "add_event/add_event.html",
                controller: 'AddEventCtrl'
            })
    })
    .controller('AddEventCtrl', function ($rootScope, $scope, $state) {
        var geocoder;
        var map;
        var my_latitude,my_longitude;

        $rootScope.event_authenticationError = false;
        /*        $scope.selectedDate = new Date();
         $scope.time = new Date();*/

        if (navigator.geolocation) {
            $scope.geolloc = "loading...";
            navigator.geolocation.getCurrentPosition(function (position) {
                my_latitude =position.coords.latitude;
                my_longitude=position.coords.longitude;
                $state.go('add_event');
                window.initialize();
            }, function (error) {

            });
        } else {

        }

        $scope.showAddress = function () {
            window.initialize();
            var marker = [];
            var address = $scope.address;
            var addressGeoLocation = [];
            var latlngbounds = new google.maps.LatLngBounds();

            geocoder.geocode({ 'address': address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    addressGeoLocation = results[0].geometry.location;
                    map.setCenter(results[0].geometry.location);
                    marker = new google.maps.Marker({
                        map: map,
                        draggable:true,
                        position: results[0].geometry.location
                    });

                    google.maps.event.addListener(marker, 'dragend', function () {
                        var drag_position = marker.getPosition();
                        $scope.event_latitude = drag_position['A'];
                        $scope.event_longitude = drag_position['F'];
                        $state.go('add_event');
                    });

                    latlngbounds.extend(addressGeoLocation);
                    map.fitBounds(latlngbounds);
                    $scope.event_latitude = addressGeoLocation['A'];
                    $scope.event_longitude = addressGeoLocation['F'];
                    $state.go('add_event');

                } else {
                    alert("Geocode was not successful for the following reason: " + status);
                }
            });

        };
        $scope.saveEvent = function () {
            $rootScope.dataLoaded = false;
            var Event = Parse.Object.extend("Event");
            var Attendee = Parse.Object.extend("Attendee");

            var event = new Event();

            //set geo point
            var point = new Parse.GeoPoint({
                latitude: parseFloat($scope.event_latitude),
                longitude: parseFloat($scope.event_longitude)
            });

            event.set('name', $scope.event_name);
            event.set('venue', $scope.event_v_name);
            event.set('date', $scope.selectedDate);
            event.set('time', $scope.time);
            event.set('location', point);

            var emails, email, attendee;

            if ($scope.event_ia != undefined) {
                emails = $scope.event_ia.split(',');
                for (var i in emails) {
                    email = emails[i].trim();
                    attendee = new Attendee();
                    attendee.set('event', event);
                    attendee.set('email', email);
                    attendee.save(null, {
                        success: function (attendee) {
                            $rootScope.dataLoaded = true;
                            $rootScope.event_authenticationError = false;
                            // Execute any logic that should take place after the object is saved.
                            $state.go('home');
                        },
                        error: function (attendee, error) {
                            $rootScope.event_authenticationError = true;
                            $rootScope.dataLoaded = true;
                            $state.go('add_event');
                            // Execute any logic that should take place if the save fails.
                            // error is a Parse.Error with an error code and message.
                            alert('Failed to create new object, with error code: ' + error.message);
                        }
                    });
                }
            }
            else {
                emails = '';
                email = emails;
                attendee = new Attendee();
                attendee.set('event', event);
                attendee.set('email', email);

                attendee.save(null, {
                    success: function (attendee) {
                        $rootScope.dataLoaded = true;
                        $rootScope.event_authenticationError = false;
                        // Execute any logic that should take place after the object is saved.
                        $state.go('home');
                    },
                    error: function (attendee, error) {
                        $rootScope.event_authenticationError = true;
                        $rootScope.dataLoaded = true;
                        $state.go('add_event');
                        // Execute any logic that should take place if the save fails.
                        // error is a Parse.Error with an error code and message.
                        alert('Failed to create new object, with error code: ' + error.message);
                    }
                });
            }

        }

        //load google map
        window.initialize = function () {
            geocoder = new google.maps.Geocoder();
            var latlng = new google.maps.LatLng(my_latitude,my_longitude);
            var mapOptions = {
                zoom: 8,
                center: latlng
            };
            map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);


        }

    });