'use strict';

angular.module('meetuporganizer.home', [])

    .config(function ($stateProvider) {
        $stateProvider
            .state('home', {
                url: "/home",
                templateUrl: "home/home.html",
                controller: 'HomeCtrl'
            })
    })
    .controller('HomeCtrl', function ($rootScope, $scope, $state,$filter) {
        var map;
        var filter = false;
        //get user location
        if (navigator.geolocation) {
            $scope.geolloc = "loading...";
            navigator.geolocation.getCurrentPosition(function (position) {
                $scope.user_lat = position.coords.latitude;
                $scope.user_lng = position.coords.longitude;
                $(".user_lat").attr('disabled','disabled');
                $(".user_lng").attr('disabled','disabled');
                $state.go('home');
                window.initialize();
                $scope.geolloc = "done";
            }, function (error) {
                $(".user_lat").removeAttr('disabled');
                $(".user_lng").removeAttr('disabled');
                console.log(error)
            });
        } else {
            $scope.geolloc = "not supported";
        }

        $scope.eventsList = [];

        var Event = Parse.Object.extend("Event");
        var query = new Parse.Query(Event);
        query.descending("date");

        query.find({
            success: function (results) {
                $scope.eventsList = results;
                window.initialize();
            },
            error: function (error) {
                alert("Error: " + error.code + " " + error.message);
            }
        });

        var circle = null;

        $scope.filter = function () {
            var userLocation = new Parse.GeoPoint({
                latitude: parseFloat($scope.user_lat),
                longitude: parseFloat($scope.user_lng)
            });

            var query = new Parse.Query(Event);
            query.descending("date");
            query.withinKilometers("location", userLocation, parseFloat($scope.user_range));

            query.find({
                success: function (results) {
                    filter = true;
                    $scope.eventsList = results;
                    window.initialize();
                    //draw circle
                    circle.setMap(map);
                    circle.setCenter(new google.maps.LatLng(parseFloat($scope.user_lat), parseFloat($scope.user_lng)));
                    circle.setRadius($scope.user_range * 1000);
                    map.fitBounds(circle.getBounds());
                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });

        }

        var markers = [];

        var updateMap = function () {
        	for(var j in markers){
        		markers[j].setMap(null);
        	}

            markers = [];

            //add markers
            var latlngbounds = new google.maps.LatLngBounds();

            var opendInfoWindow = false;

            for (var i in $scope.eventsList) {
                var location = $scope.eventsList[i].get('location');
                var addressGeoLocation = new google.maps.LatLng(location.latitude, location.longitude);
                latlngbounds.extend(addressGeoLocation);

                var infowindow = new google.maps.InfoWindow({
                    content: $scope.eventsList[i].get('name')
                });

                var marker = new google.maps.Marker({
                    map: map,
                    position: addressGeoLocation,
                    animation: google.maps.Animation.DROP,
                    infowindow: infowindow
                });
                createTooltip(marker,i);
                markers.push(marker)
            }
            if(!filter){
                map.fitBounds(latlngbounds);
            }

        };
        var lastOpenInfoWin = null;
        function createTooltip(marker,i) {
            //create a tooltip
            var tooltipOptions = new google.maps.InfoWindow({
                marker: marker,
                content: "<h3>"+$scope.eventsList[i].get('name')+"</h3><div><stong>Date:</strong>"+$filter('date')($scope.eventsList[i].get('date'), 'MMMM d, y')+"</div>" +
                    "<div><stong>Venue:</strong>"+$scope.eventsList[i].get('venue')+"</div>" +
                    "<div><stong>Time:</strong>"+$filter('date')($scope.eventsList[i].get('time'), 'h:mm a')+"</div>",
                cssClass: 'tooltip' // name of a css class to apply to tooltip
            });
            //open infowindo on click event on marker.
            google.maps.event.addListener(marker, 'mouseover', function () {
                /*if (lastOpenInfoWin) lastOpenInfoWin.close();*/
                lastOpenInfoWin = tooltipOptions;
                tooltipOptions.open(marker.get('map'), marker);
            });
            google.maps.event.addListener(marker, 'mouseout', function () {
                lastOpenInfoWin = tooltipOptions;
                tooltipOptions.close();
            });

        }

        //load google map
        window.initialize = function () {
            var address = [];

            //map style
            var styles = [
                {
                    stylers: [
                        {
                            hue: "#00ffe6"
                        },
                        {
                            saturation: -20
                        }
                    ]
                },
                {
                    featureType: "road",
                    elementType: "geometry",
                    stylers: [
                        {
                            lightness: 100
                        },
                        {
                            visibility: "simplified"
                        }
                    ]
                },
                {
                    featureType: "road",
                    elementType: "labels",
                    stylers: [
                        {
                            visibility: "off"
                        }
                    ]
                }
            ];

            var styledMap = new google.maps.StyledMapType(styles, { name: "Default Style" });
            var geocoder = new google.maps.Geocoder();
            var mapOptions = {
                zoom: 10,
                center: new google.maps.LatLng(45.50, -98.35),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);


            map.mapTypes.set('map_style', styledMap);
            map.setMapTypeId('map_style');

            var query = new Parse.Query(Event);
            query.descending("date");

            //circle design
            circle = new google.maps.Circle({
		      strokeColor: '#FF0000',
		      strokeOpacity: 0.8,
		      strokeWeight: 1,
		      fillColor: '#FF0000',
		      fillOpacity: 0.2,
		      map: null,
		      center: new google.maps.LatLng(0, 0),
		      radius: 1
		    });

            query.find({
                success: function (results) {
                    $scope.eventsList = results;
                    updateMap();
                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });
        }

    });