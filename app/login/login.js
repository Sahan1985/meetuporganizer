'use strict';

angular.module('meetuporganizer.login', [])

    .config(function ($stateProvider) {
        $stateProvider
            .state('login', {
                url: "/login",
                templateUrl: "login/login.html",
                controller: 'LoginCtrl'
            })
    })

    .controller('LoginCtrl', function ($rootScope, $scope, $state) {
        $rootScope.authenticationError = false;
        /*login using parse start here*/
        $scope.user = {
            email: 'admin@localhost.com',
            password: ''
        };

        $scope.login = function () {
            $rootScope.dataLoaded = false;
            /*parse login method*/
            Parse.User.logIn($scope.user.email, $scope.user.password, {
                success: function (user) {
                    $rootScope.$apply(function () {
                        $rootScope.currentUser = Parse.User.current();
                        $rootScope.isLoggedIn = true;
                        $rootScope.dataLoaded = true;
                        $rootScope.authenticationError = false;
                        $state.go('home');
                    });

                },
                error: function (user, error) {
                    $rootScope.authenticationError = true;
                    $rootScope.dataLoaded = true;
                    $state.go('login');

                }
            });

        };
        /*login using parse End here*/
    });