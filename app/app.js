'use strict';

Parse.initialize("qwIS7YpqvnKoXb6Uwa68blgVVT3qZMN6neVZO6kB",
  "u5oaRksvIdNRwISab0IoZ2COV59lAKSdsYMz9XFN");

// Declare app level module which depends on views, and components
angular.module('meetuporganizer', [
  'ngRoute',
  'ui.router',
  'meetuporganizer.home',
  'meetuporganizer.login',
  'meetuporganizer.logout',
  'meetuporganizer.signup',
  'meetuporganizer.add_event',
  'meetuporganizer.version',
  'mgcrea.ngStrap',
  'ngMessages',
  'angular-country-picker'
])

.controller('MainAppCtrl', ['$scope', '$rootScope', function($scope, $rootScope) {
  //restore parse session
  $rootScope.currentUser = Parse.User.current();
  $rootScope.dataLoaded = true;

}])
.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise("/home");

});