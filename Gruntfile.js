module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */'
      },
      build: {
        src: 'app/build/app.js',
        dest: 'app/build/app.min.js'
      }
    },

    concat: {
      options: {
        separator: ';\n',
        stripBanners: {
          block: true,
          line: true
        }
      },
      app: {
        src: [
          'app/bower_components/jquery/dist/jquery.js',
          'app/bower_components/bootstrap/dist/js/bootstrap.js',
          'app/bower_components/angular/angular.js',
          'app/bower_components/angular-ui-router/release/angular-ui-router.js',
          'app/bower_components/angular-route/angular-route.js',
          'app/bower_components/angular-strap/dist/angular-strap.min.js',
          'app/bower_components/angular-strap/dist/angular-strap.tpl.js',
          'app/bower_components/angular-messages/angular-messages.js',
          'app/bower_components/angular-country-picker/country-picker.js',
          'app/bower_components/parse-angular-patch/src/parse-angular.js',
          'app/app.js',
          'app/home/home.js',
          'app/login/login.js',
          'app/logout/logout.js',
          'app/signup/signup.js',
          'app/add_event/add_event.js',
          'app/components/version/version.js',
          'app/components/version/version-directive.js',
          'app/components/version/interpolate-filter.js'
        ],
        dest: 'app/build/app.js'
      }
    },

    watch: {
      configFiles: {
        files: ['Gruntfile.js'],
        options: {
          reload: true,
          livereload: true
        }
      },
      app: {
        files: ['app/*', 'app/**/*.*'],
        tasks: ['concat'],
        options: {
          livereload: true
        }
      }
    },

    cssmin: {
      options: {
        shorthandCompacting: false,
        roundingPrecision: -1,
        sourceMap: true,
        keepSpecialComments: 0
      },
      target: {
        files: {
          'app/build/style.css': [
            'app/app.css'
          ]
        }
      }
    }

  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-concat');

  grunt.registerTask('default', ['watch']);

  grunt.registerTask('dev', ['concat', 'cssmin']);
  grunt.registerTask('prod', ['concat', 'cssmin', 'uglify']);
};